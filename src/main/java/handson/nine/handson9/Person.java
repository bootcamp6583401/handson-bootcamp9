package handson.nine.handson9;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Person implements Serializable {
    // private static final long serialVersionUID = 5L;
    private static final int ver = 4;
    private String name;
    private int age;
    private String occupation;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
        this.occupation = "Accounting";
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getOccupation() {
        return occupation;
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        System.out.println("READ!!!");

        // Check the version of the serialized data
        int serializedVersion = in.readInt();
        switch (serializedVersion) {
            case 1:
                System.out.println("Version 1");
                break;
            case 2:
                System.out.println("Version 2!");
                break;
            case 3:
                System.out.println("Version 3!");
                break;
            case 4:
                System.out.println("Version 4!");
                break;
            default:
                throw new IOException("Unsupported version: " + serializedVersion);
        }
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();

        out.writeInt(Person.ver);

    }
}
