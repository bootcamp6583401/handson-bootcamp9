package handson.nine.handson9;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("What do you want to do? ");
        System.out.println("1. Serialize to task2.txt");
        System.out.println("2. Deserialize from task2.txt");

        int answer = scanner.nextInt();

        Person person = new Person("Kevin Malone", 33);
        switch (answer) {
            case 1:
                Task1.serializePerson(person, "task2.txt");
                break;
            case 2:
                Person deserializedPerson = Task1.deserializePerson("task2.txt");
                System.out.println(deserializedPerson.getName());
                break;
            default:
                break;
        }

        scanner.close();

    }

}
