package handson.nine.handson9;

import java.io.*;

public class Task1 {
    public static void main(String[] args) {
        Person person = new Person("Jim Halpert", 25);

        serializePerson(person, "person.txt");

        Person restoredPerson = deserializePerson("person.txt");

        if (restoredPerson != null) {
            System.out.println("Restored Person: " + restoredPerson.getName() + ", Age: " + restoredPerson.getAge());
        }
    }

    public static void serializePerson(Person person, String filename) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filename))) {
            oos.writeObject(person);
            System.out.println("Person object serialized and saved to " + filename);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Person deserializePerson(String filename) {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename))) {
            Person person = (Person) ois.readObject();
            System.out.println("Person object deserialized from " + filename);
            return person;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

}
