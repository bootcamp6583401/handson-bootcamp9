package handson.nine.handson9;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Task1Test {

    public static Person person;

    @DisplayName("Test Serialization")
    @Test
    void serializationCorrect() {

        Task1.serializePerson(person, "test.txt");

        Person deserializedPerson = Task1.deserializePerson("test.txt");

        assertNotNull(deserializedPerson);

        assertEquals(person.getName(), deserializedPerson.getName());
        assertEquals(person.getAge(), deserializedPerson.getAge());
    }

    @BeforeAll
    static void setup() {
        person = new Person("Michael Scott", 47);

        System.out.println("TESTESTESTESTESTESTESTESTEST");
    }

}
